README for src-other.git
========================

This is a collection of Makefiles to quickly clone, download, install or 
upgrade software packages, inspired by ports and pkgsrc on various BSD 
systems. All packages build on (GNU/)Linux, and most of them also 
compile and install on FreeBSD, NetBSD and OpenBSD. The Makefiles are 
written with portability in mind, so no functionality exclusive to e.g. 
GNU Make or Berkeley Make is used. If any errors occur, the make command 
is aborted.

Directory structure
-------------------

Everything is installed under the directory specified by the `TOP` 
variable, it corresponds to the common `--prefix` option used by e.g. 
`configure` from GNU Autoconf. The default value is `/usr/src-other`.

To install or upgrade to another location, for example if there's no 
root access and you want to install everything under `~/local/`, execute

    make install TOP=~/local

Or, to upgrade existing packages using the standard `/usr/local/` 
location:

    make fullclean upgrade TOP=/usr/local

`TOP` is not set to this value by default, to avoid conflicts with 
existing software. This makes it easy to test out new or old versions of 
the software without overwriting anything under /usr/local/.

Under `$TOP`, the packages are installed to the `pool/` directory, in a 
directory consisting of package name, branch name if it's from a version 
control system, and version number with commit ID if it's available.

The other special directory under `$TOP` is `prg/`. This directory 
contains symlinks to the current installed version under `$TOP/pool/`. 
To use another installed version of the software, it's enough to update 
these links to point to the new version.

After a successful `make install` or `make upgrade`, the `$TOP` subtree 
is populated with the necessary symlinks at the standard locations in 
the same way it's done under `/usr/local/`.

To be able to run all the installed programs, add `$TOP/bin` (usually 
`/usr/src-other` unless it's installed with a modified `TOP` value) to 
your `PATH`. For example:

`export PATH=/usr/src-other/bin:`*[...]*

Install a different version
---------------------------

For most packages, the newest revision on the stable branch is chosen 
(usually the `master` branch), but if you want to install an older 
version or a specific commit or branch, specify the commit with the 
`REF` variable. For example, to install various versions of Git, chdir 
to the `devel/git/` directory and run something like this:

    make install REF=v2.5.0

or

    make update REF=787f75f0567a

or

    make upgrade REF=mybranch TEST=1

etc.

Any changes to the build can of course be made by editing the Makefiles 
or override values by defining them on the command line.

Global `make` commands
----------------------

These commands work in every directory. If there's no action available 
for a specific make command in some packages (projects), for example if 
there's no working test suite there, the command is ignored for that 
package.

- **make**\
  Build (but don't install) all packages in all subdirectories. An alias 
  for this is `make build`.
- **make clean**\
  Run "make clean" in all packages below the current directory.
- **make distclean**\
  Run "make distclean" or similar in all packages below the current 
  directory. This doesn't guarantee that all repositories or package 
  directories are set to a pristine condition, it all depends on the 
  upstream projects.
- **make fullclean**\
  The nuclear option, executes `git reset --hard` and `git clean -fdx`. 
  All local modifications and unknown files/directories will ble deleted 
  and reset to the pristine upstream version. To make scripting easier, 
  no confirmation is done. Make sure to commit all modifications you 
  want to keep before running this command.
- **make install**\
  Install all packages in projects below the current subdirectory. 
- **make test**\
  Run the test suite in all packages below the current directory. The 
  actual command may vary in the various packages, but this command will 
  always translate into the correct test command.
- **make update**\
  Fetch the newest revisions from the repositories in the current 
  subdirectory tree.
- **make upgrade**\
  For each package in the subdirectory tree, upgrade to the newest 
  upstream version. If the package isn't installed or the newest version 
  already is installed under `$TOP/pool/`, nothing is done. To run the 
  various test suites before installation, define the `TEST` variable. 
  For example: `make fullclean upgrade TEST=1`.
- **make svg**\
  Generate svg diagrams. For now, it's only `relations.svg`.

Local `make` commands
---------------------

These commands may vary among the various packages, but they behave 
identically all places they are enabled.

- **make clone**\
  Create a updated clone of the repository.
- **make check-remotes**\
  Define repository remotes and fetch commits from the upstream 
  repository.
- **make targz**\
  If there's no version control for a specific package, download the 
  `.tar.gz` or `.zip` file from upstream, or copy it from 
  `~/repos/Files/` if it's there.
- **make create-configure**\
  Generate the `configure` or similar script.
- **make configure**\
  Execute a command that corresponds to `./configure` with the necessary 
  arguments.

Examples
--------

Install all packages under the current directory to the default 
directory tree `/usr/src-other`:

    make install

Same, but also run tests and install to an alternative location in the 
home directory:

    make test install TOP=~/local

Upgrade existing packages from a pristine upstream copy and run tests:

    make fullclean upgrade TEST=1

Don't install anything, just fetch repository commits for all packages 
under the current directory:

    make update

Development
-----------

The `master` branch is considered stable and will never be rebased. 
Every new functionality or bug fix is created on topic branches which 
may be rebased now and then. No intentional breakage on `master` is 
allowed. Commits that break the build on `master` must be squashed into 
one atomic, non-breaking commit.

Download
--------

The project page is at &lt;<https://gitlab.com/sunny256/src-other>&gt; 
and it can be cloned from `git@gitlab.com:sunny256/src-other.git` or 
&lt;<https://gitlab.com/sunny256/src-other.git>&gt;.

Author
------

Øyvind A. Holm &lt;<sunny@sunbase.org>&gt;

License
-------

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the 
Free Software Foundation; either version 2 of the License, or (at your 
option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along 
with this program.
If not, see &lt;<http://www.gnu.org/licenses/>&gt;.

----

    File ID: 92f2f8ac-e1d3-11e6-b760-f74d993421b0
    vim: set ts=2 sw=2 sts=2 tw=72 et fo=tcqw fenc=utf8 :
    vim: set com=b\:#,fb\:-,fb\:*,n\:> ft=markdown :
