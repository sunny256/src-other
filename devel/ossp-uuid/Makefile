# File ID: 17ef3292-b09b-11e6-ad2b-279c2a0468a3
# Author: Øyvind A. Holm <sunny@sunbase.org>

PACKAGE = ossp-uuid
PROGVER = 1.6.2
FNAME = uuid-$(PROGVER)
TARGZ = $(FNAME).tar.gz
PROGBASE = uuid

CONFIGURE_FLAGS =
ANNEX_DIR = $(HOME)/annex/musikk/software/diverse
UPSTREAM_URL = ftp://ftp.ossp.org/pkg/lib/uuid/$(TARGZ)

PROG = $(FNAME)/$(PROGBASE)
TARGZ_DEST = $(FNAME)/configure
CONFIGURE_DEST = $(FNAME)/config.log

TOP = /usr/src-other
TOPPROG = $(TOP)/bin/$(PROGBASE)

PRG = $(TOP)/prg
PRGNAME = $(PRG)/$(PACKAGE)
PRGPROG = $(PRGNAME)/bin/$(PROGBASE)

POOL = $(TOP)/pool
PREFIX = $(POOL)/$(PACKAGE)-$(PROGVER)
PREFPROG = $(PREFIX)/bin/$(PROGBASE)

.PHONY: default
default: build

.PHONY: install
install: $(TOPPROG)

$(TOPPROG): $(PRGPROG)
	cd $(PRGNAME) && find . -type d | while read f; do \
		mkdir -p "$(TOP)/$$f"; \
	done
	cd $(PRGNAME) && (find . -type f; find . -type l) | while read f; do \
		ln -fns "$(PRGNAME)/$$f" "$(TOP)/$$f"; \
	done

$(PRGPROG): $(PREFPROG)
	mkdir -p $(PRG)
	ln -fns $(PREFIX) $(PRGNAME)

$(PREFPROG): $(PROG)
	cd $(FNAME) && $(MAKE) install

.PHONY: build
build: $(PROG)

$(PROG): $(CONFIGURE_DEST)
	cd $(FNAME) && $(MAKE)

.PHONY: configure
configure: $(CONFIGURE_DEST)

$(CONFIGURE_DEST): $(TARGZ_DEST)
	cd $(FNAME) && ./configure --prefix=$(PREFIX) $(CONFIGURE_FLAGS)

.PHONY: upgrade
upgrade:

.PHONY: extract
extract: $(TARGZ_DEST)

$(TARGZ_DEST): $(TARGZ)
	tar xzf $(TARGZ)
	touch $(TARGZ_DEST)

.PHONY: targz
targz: $(TARGZ)

$(TARGZ):
	wget $(UPSTREAM_URL) -O $(TARGZ)
	touch $(TARGZ)

.PHONY: clean
clean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) clean; fi

.PHONY: distclean
distclean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) distclean; fi

.PHONY: fullclean
fullclean:
	rm -fr $(FNAME)

.PHONY: test
test: build
	cd -P $(FNAME) && $(MAKE) check

.PHONY: update
update:
