# File ID: 5c51425e-2ea9-11e7-aa10-db5caa6d21d3
# Author: Øyvind A. Holm <sunny@sunbase.org>

PACKAGE = ledger
FNAME = $(PACKAGE)
PROGBASE = $(PACKAGE)

REF = v3.2.1
CONFIGURE_FLAGS =

UPSTREAM_URL = git://github.com/ledger/ledger.git

PROG = $(FNAME)/$(PROGBASE)
CLONE_DEST = $(FNAME)/.git/HEAD
CONFIGURE_DEST = $(FNAME)/CMakeFiles/Makefile.cmake

TOP = /usr/src-other
TOPPROG = $(TOP)/bin/$(PROGBASE)

PRG = $(TOP)/prg
PRGNAME = $(PRG)/$(PACKAGE)
PRGPROG = $(PRGNAME)/bin/$(PROGBASE)

POOL = $(TOP)/pool
PREF = $(POOL)/$(PACKAGE).$(REF).

.PHONY: default
default: build

.PHONY: install
install: $(TOPPROG)

$(TOPPROG): $(PRGPROG)
	cd $(PRGNAME) && find . -type d | while read f; do \
		mkdir -p "$(TOP)/$$f"; \
	done
	cd $(PRGNAME) && (find . -type f; find . -type l) | while read f; do \
		ln -fns "$(PRGNAME)/$$f" "$(TOP)/$$f"; \
	done

$(PRGPROG): $(PROG)
	mkdir -p $(POOL)
	cd $(FNAME) && $(MAKE) install
	mkdir -p $(PRG)
	cd $(FNAME) && ln -fns $(PREF)$$(git describe --tags --long) $(PRGNAME)

.PHONY: build
build: $(PROG)

$(PROG): $(CONFIGURE_DEST)
	cd $(FNAME) && $(MAKE)

.PHONY: configure
configure: $(CONFIGURE_DEST)

$(CONFIGURE_DEST): $(CLONE_DEST)
	cd $(FNAME) && ./acprep --prefix=$(PREF)$$( \
	    git describe --tags --long \
	  ) $(CONFIGURE_FLAGS)

.PHONY: upgrade
upgrade:
	if test ! -e "$(PRGPROG)"; then \
		echo $(PACKAGE) is not installed here >&2; \
		exit 0; \
	else \
		$(MAKE) distclean update || exit 1; \
		cd $(FNAME) && \
		    if test -e "$(PREF)$$( \
		        git describe --tags --long \
		      )/bin/$(PROGBASE)"; then \
			echo This $(PACKAGE) version is already installed >&2; \
		else \
			cd .. || exit 1; \
			if test -n "$(TEST)"; then \
				$(MAKE) test || exit 1; \
			fi; \
			$(MAKE) install || exit 1; \
		fi; \
	fi

.PHONY: update
update: $(CLONE_DEST) check-remotes
	cd $(FNAME) && git checkout $$(git rev-parse HEAD)
	cd $(FNAME) && git push . origin/master:master
	cd $(FNAME) && git branch -u origin/master master
	cd $(FNAME) && git push . origin/next:next
	cd $(FNAME) && git branch -u origin/next next
	cd $(FNAME) && git checkout $(REF)

.PHONY: check-remotes
check-remotes:
	cd $(FNAME) && git remote | grep -q ^origin || \
		git remote add origin $(UPSTREAM_URL)
	cd $(FNAME) && git fetch origin

.PHONY: clone
clone: $(CLONE_DEST)

$(CLONE_DEST):
	git clone -b next $(UPSTREAM_URL) $(FNAME)
	$(MAKE) update

.PHONY: clean
clean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) clean; fi

.PHONY: distclean
distclean:
	# It doesn't have distclean, execute regular "make clean"
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) clean; fi

.PHONY: fullclean
fullclean:
	if test -d $(FNAME)/.git; then \
		cd $(FNAME) && git reset --hard && git clean -dfx || exit 1; \
	fi

.PHONY: test
test: build
	cd -P $(FNAME) && $(MAKE) test
