# File ID: 334f2a02-43bb-11e7-9a3b-f74d993421b0
# Author: Øyvind A. Holm <sunny@sunbase.org>

PACKAGE = suuid
FNAME = $(PACKAGE)
PROGBASE = $(PACKAGE)

REF = master
CONFIGURE_FLAGS =

UPSTREAM_URL = https://gitlab.com/sunny256/suuid.git

PROG = $(FNAME)/src/$(PROGBASE)
CLONE_DEST = $(FNAME)/.git/HEAD

TOP = /usr/src-other
TOPPROG = $(TOP)/bin/$(PROGBASE)

PRG = $(TOP)/prg
PRGNAME = $(PRG)/$(PACKAGE)
PRGPROG = $(PRGNAME)/bin/$(PROGBASE)

POOL = $(TOP)/pool
PREF = $(POOL)/$(PACKAGE).$(REF).

.PHONY: default
default: build

.PHONY: install
install: $(TOPPROG)

$(TOPPROG): $(PRGPROG)
	cd $(PRGNAME) && find . -type d | while read f; do \
		mkdir -p "$(TOP)/$$f"; \
	done
	cd $(PRGNAME) && (find . -type f; find . -type l) | while read f; do \
		ln -fns "$(PRGNAME)/$$f" "$(TOP)/$$f"; \
	done

$(PRGPROG): $(PROG)
	cd $(FNAME) && $(MAKE) install PREFIX=$(PREF)$$(git describe --long)
	cd $(FNAME) && ln -fns $(PREF)$$(git describe --long) $(PRGNAME)

.PHONY: build
build: $(PROG)

$(PROG): $(CLONE_DEST)
	cd $(FNAME) && $(MAKE)

.PHONY: configure
configure: $(CLONE_DEST)

.PHONY: upgrade
upgrade:
	if test ! -e "$(PRGPROG)"; then \
		echo $(PACKAGE) is not installed here >&2; \
		exit 0; \
	else \
		$(MAKE) distclean update || exit 1; \
		cd $(FNAME) && \
		    if test -e "$(PREF)$$( \
		        git describe --long \
		      )/bin/$(PROGBASE)"; then \
			echo This $(PACKAGE) version is already installed >&2; \
		else \
			cd .. || exit 1; \
			if test -n "$(TEST)"; then \
				$(MAKE) test || exit 1; \
			fi; \
			$(MAKE) install || exit 1; \
		fi; \
	fi

.PHONY: update
update: $(CLONE_DEST) check-remotes
	cd $(FNAME) && git checkout $$(git rev-parse HEAD)
	cd $(FNAME) && git push . origin/master:master
	cd $(FNAME) && git branch -u origin/master master
	cd $(FNAME) && git checkout $(REF)

.PHONY: check-remotes
check-remotes:
	cd $(FNAME) && git remote | grep -q ^origin || \
		git remote add origin $(UPSTREAM_URL)
	cd $(FNAME) && git fetch origin

.PHONY: clone
clone: $(CLONE_DEST)

$(CLONE_DEST):
	git clone $(UPSTREAM_URL) $(FNAME)
	$(MAKE) update

.PHONY: clean
clean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) clean; fi

.PHONY: distclean
distclean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) distclean; fi

.PHONY: fullclean
fullclean:
	if test -d $(FNAME)/.git; then \
		cd $(FNAME) && git reset --hard && git clean -dfx || exit 1; \
	fi

.PHONY: test
test: build
	cd -P $(FNAME) && $(MAKE) test
