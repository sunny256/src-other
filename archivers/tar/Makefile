# File ID: 78a2cee8-61c4-11e7-8308-f74d993421b0
# Author: Øyvind A. Holm <sunny@sunbase.org>

PACKAGE = tar
FNAME = $(PACKAGE)
PROGBASE = $(PACKAGE)

REF = release_1_33
GNULIBDIR =
BOOTSTRAP_FLAGS = --skip-po
CONFIGURE_FLAGS =

UPSTREAM_URL = git://git.sv.gnu.org/tar.git

PROG = $(FNAME)/src/$(PROGBASE)
CLONE_DEST = $(FNAME)/.git/HEAD
CONFIGURE_DEST = $(FNAME)/config.log

TOP = /usr/src-other
TOPPROG = $(TOP)/bin/$(PROGBASE)

PRG = $(TOP)/prg
PRGNAME = $(PRG)/$(PACKAGE)
PRGPROG = $(PRGNAME)/bin/$(PROGBASE)

POOL = $(TOP)/pool
PREF = $(POOL)/$(PACKAGE).$(REF).

.PHONY: default
default: build

.PHONY: install
install: $(TOPPROG)

$(TOPPROG): $(PRGPROG)
	cd $(PRGNAME) && find . -type d | while read f; do \
		mkdir -p "$(TOP)/$$f"; \
	done
	cd $(PRGNAME) && (find . -type f; find . -type l) | while read f; do \
		ln -fns "$(PRGNAME)/$$f" "$(TOP)/$$f"; \
	done

$(PRGPROG): $(PROG)
	mkdir -p $(POOL)
	cd $(FNAME) && $(MAKE) install
	mkdir -p $(PRG)
	cd $(FNAME) && ln -fns $(PREF)$$(git describe --long) $(PRGNAME)

.PHONY: build
build: $(PROG)

$(PROG): $(CONFIGURE_DEST)
	cd $(FNAME) && $(MAKE)

.PHONY: configure
configure: $(CONFIGURE_DEST)

$(CONFIGURE_DEST): $(FNAME)/configure
	cd $(FNAME) && ./configure --prefix=$(PREF)$$( \
	    git describe --long \
	  ) $(CONFIGURE_FLAGS)

.PHONY: create-configure
create-configure: $(FNAME)/configure

$(FNAME)/configure: $(CLONE_DEST)
	if test -n "$(GNULIBDIR)"; then \
		cd $(FNAME) && \
			./bootstrap $(BOOTSTRAP_FLAGS) \
			    --gnulib-srcdir=$(GNULIBDIR) || exit 1; \
	else \
		cd $(FNAME) && ./bootstrap $(BOOTSTRAP_FLAGS) || exit 1; \
	fi

.PHONY: upgrade
upgrade:
	if test ! -e "$(PRGPROG)"; then \
		echo $(PACKAGE) is not installed here >&2; \
		exit 0; \
	else \
		$(MAKE) distclean update || exit 1; \
		cd $(FNAME) && \
		    if test -e "$(PREF)$$( \
		        git describe --long \
		      )/bin/$(PROGBASE)"; then \
			echo This $(PACKAGE) version is already installed >&2; \
		else \
			cd .. || exit 1; \
			if test -n "$(TEST)"; then \
				$(MAKE) test || exit 1; \
			fi; \
			$(MAKE) install || exit 1; \
		fi; \
	fi

.PHONY: update
update: $(CLONE_DEST) check-remotes
	cd $(FNAME) && git checkout $$(git rev-parse HEAD)
	cd $(FNAME) && git push . origin/master:master
	cd $(FNAME) && git branch -u origin/master master
	cd $(FNAME) && git checkout $(REF)
	cd $(FNAME) && git submodule init
	cd $(FNAME) && git submodule update

.PHONY: check-remotes
check-remotes:
	cd $(FNAME) && git remote | grep -q ^origin || \
		git remote add origin $(UPSTREAM_URL)
	cd $(FNAME) && git fetch origin

.PHONY: clone
clone: $(CLONE_DEST)

$(CLONE_DEST):
	git clone -c "fetch.fsckObjects=false" $(UPSTREAM_URL) $(FNAME)
	$(MAKE) update

.PHONY: clean
clean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) clean; fi

.PHONY: distclean
distclean:
	if test -f $(FNAME)/Makefile; then cd $(FNAME) && $(MAKE) distclean; fi

.PHONY: fullclean
fullclean:
	if test -d $(FNAME)/.git; then \
		cd $(FNAME) && \
		    git reset --hard && git clean -dfx && \
		    git submodule foreach git reset --hard && \
		    git submodule foreach git clean -dfx || exit 1; \
	fi

.PHONY: test
test: build
	cd -P $(FNAME) && $(MAKE) check
