# File ID: 30c157ba-ac26-11e6-9e34-279c2a0468a3
# Author: Øyvind A. Holm <sunny@sunbase.org>

.PHONY: default
default:
	cd archivers && $(MAKE)
	cd databases && $(MAKE)
	cd devel && $(MAKE)
	cd editors && $(MAKE)
	cd emulators && $(MAKE)
	cd finance && $(MAKE)
	cd geography && $(MAKE)
	cd mail && $(MAKE)
	cd multimedia && $(MAKE)
	cd net && $(MAKE)
	cd sysutils && $(MAKE)
	cd textproc && $(MAKE)

.PHONY: clean
clean:
	rm -f relations.svg
	cd archivers && $(MAKE) clean
	cd databases && $(MAKE) clean
	cd devel && $(MAKE) clean
	cd editors && $(MAKE) clean
	cd emulators && $(MAKE) clean
	cd finance && $(MAKE) clean
	cd geography && $(MAKE) clean
	cd mail && $(MAKE) clean
	cd multimedia && $(MAKE) clean
	cd net && $(MAKE) clean
	cd sysutils && $(MAKE) clean
	cd textproc && $(MAKE) clean

.PHONY: distclean
distclean:
	cd archivers && $(MAKE) distclean
	cd databases && $(MAKE) distclean
	cd devel && $(MAKE) distclean
	cd editors && $(MAKE) distclean
	cd emulators && $(MAKE) distclean
	cd finance && $(MAKE) distclean
	cd geography && $(MAKE) distclean
	cd mail && $(MAKE) distclean
	cd multimedia && $(MAKE) distclean
	cd net && $(MAKE) distclean
	cd sysutils && $(MAKE) distclean
	cd textproc && $(MAKE) distclean

.PHONY: edit
edit:
	$(EDITOR) $$(git ls-files)

.PHONY: fullclean
fullclean:
	cd archivers && $(MAKE) fullclean
	cd databases && $(MAKE) fullclean
	cd devel && $(MAKE) fullclean
	cd editors && $(MAKE) fullclean
	cd emulators && $(MAKE) fullclean
	cd finance && $(MAKE) fullclean
	cd geography && $(MAKE) fullclean
	cd mail && $(MAKE) fullclean
	cd multimedia && $(MAKE) fullclean
	cd net && $(MAKE) fullclean
	cd sysutils && $(MAKE) fullclean
	cd textproc && $(MAKE) fullclean

.PHONY: install
install:
	cd archivers && $(MAKE) install
	cd databases && $(MAKE) install
	cd devel && $(MAKE) install
	cd editors && $(MAKE) install
	cd emulators && $(MAKE) install
	cd finance && $(MAKE) install
	cd geography && $(MAKE) install
	cd mail && $(MAKE) install
	cd multimedia && $(MAKE) install
	cd net && $(MAKE) install
	cd sysutils && $(MAKE) install
	cd textproc && $(MAKE) install

.PHONY: svg
svg: relations.svg

.PHONY: test
test:
	cd archivers && $(MAKE) test
	cd databases && $(MAKE) test
	cd devel && $(MAKE) test
	cd editors && $(MAKE) test
	cd emulators && $(MAKE) test
	cd finance && $(MAKE) test
	cd geography && $(MAKE) test
	cd mail && $(MAKE) test
	cd multimedia && $(MAKE) test
	cd net && $(MAKE) test
	cd sysutils && $(MAKE) test
	cd textproc && $(MAKE) test

.PHONY: update
update:
	cd archivers && $(MAKE) update
	cd databases && $(MAKE) update
	cd devel && $(MAKE) update
	cd editors && $(MAKE) update
	cd emulators && $(MAKE) update
	cd finance && $(MAKE) update
	cd geography && $(MAKE) update
	cd mail && $(MAKE) update
	cd multimedia && $(MAKE) update
	cd net && $(MAKE) update
	cd sysutils && $(MAKE) update
	cd textproc && $(MAKE) update

.PHONY: upgrade
upgrade:
	cd archivers && $(MAKE) upgrade
	cd databases && $(MAKE) upgrade
	cd devel && $(MAKE) upgrade
	cd editors && $(MAKE) upgrade
	cd emulators && $(MAKE) upgrade
	cd finance && $(MAKE) upgrade
	cd geography && $(MAKE) upgrade
	cd mail && $(MAKE) upgrade
	cd multimedia && $(MAKE) upgrade
	cd net && $(MAKE) upgrade
	cd sysutils && $(MAKE) upgrade
	cd textproc && $(MAKE) upgrade

relations.svg: relations.gv
	dot -Tsvg relations.gv >relations.svg
